import Layout from '../components/Layout'

const Home = () => (
  <Layout>
    <h1>
    Consequence Fullstack Assessment 
    </h1>

    <p>
    TrueLayer (https://truelayer.com/) is an open banking platform that allows integration between banking providers and fintech companies. 
    <br/>
    For example, using TrueLayer’s auth and data API, you are able to let users authorize your service to access their banking data.

    <br/>
    <br/>

    Develop a project that integrates with Truelayer (https://truelayer.com/), where:
    <br/>

    On a webpage:
    <br/>
    A user can register an account with:
    <br/>
    A username
    <br/>
    A password
    <br/>
    An email
    <br/>
    A user can change their details
    <br/>
    A user can login and logout
    <br/>
    A user can authorize access for UK bank accounts and cards using TrueLayer
    <br/>
    On a backend server:
    <br/>
    The system is able to asynchronously download and store the following data authorized by the user:
    <br/>
    Accounts:
    <br/>
    Details
    <br/>
    List of transactions
    <br/>
    Cards:
    <br/>
    Details
    <br/>
    List of transactions
    <br/>
    Retrieve new data from time to time on demand without the need for the user to reauthenticate
    <br/>
    <br/>

    Only authorization and fetching of data requires the use of TrueLayer’s API. Everything else you’ll have to implement yourself
    <br/>
    <br/>

    Other requirements:
    <br/>

    We will not judge your submission based on aesthetics, but on the functional implementation and organization of your solution.
    <br/>
    Push your code to a public repository that can be checked.
    <br/>
    Deploy your code in a Linux server that @Victor will provide. 
    <br/>
    Provide an SSH public key so you can have access to this server.
    <br/>
    You’ll have sudo privileges in this server so you can install anything you need to deploy your solution
    <br/>
    Only port 22 (SSH) and port 80 (HTTP) will be open on this server.
    <br/>
    It is highly recommended that you use Python/Django and/or Vue/Nuxt for your solution, but feel free to use any other tech stack.
    <br/>
    <br/>

    </p>
  </Layout>
)

export default Home
