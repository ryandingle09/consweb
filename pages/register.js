import { useState } from 'react'
import Head from 'next/head'
import useUser from '../lib/useUser'
import Layout from '../components/Layout'
import FormRegister from '../components/FormRegister'
import fetchJson from '../lib/fetchJson'

const Register = () => {
  const { mutateUser } = useUser({
    redirectTo: '/dashboard',
    redirectIfFound: true,
  })

  const [errorMsg, setErrorMsg] = useState('')

  async function handleSubmit(e) {
    e.preventDefault()

    const body = {
      username: e.currentTarget.username.value,
      email: e.currentTarget.email.value,
      first_name: e.currentTarget.first_name.value,
      last_name: e.currentTarget.last_name.value,
      password: e.currentTarget.password.value,
      password2: e.currentTarget.password2.value,
    }

    try {
      mutateUser(
        await fetchJson('/api/register', {
          method: 'POST',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify(body),
        })
      )
    } catch (error) {
      console.error('An unexpected error happened:', error)
      let errors = "";
      errors += (error.data.username) ? "Username: "+ error.data.username + "<br />" : '';
      errors += (error.data.email) ? "Email: "+ error.data.email + "<br />" : '';
      errors += (error.data.first_name) ? "First Name: "+ error.data.first_name + "<br />" : '';
      errors += (error.data.last_name) ? "Last Name: "+ error.data.last_name + "<br />" : '';
      errors += (error.data.password) ? "Password: "+ error.data.password + "<br />" : '';
      setErrorMsg(errors)
    }
  }

  return (
    <Layout>
      <Head>
        <title>Register | Consequence Test</title>
      </Head>
      <h1>
        <center>Registration</center>
      </h1>
      <div className="login">
        <FormRegister isLogin errorMessage={errorMsg} onSubmit={handleSubmit} />
      </div>
    </Layout>
  )
}

export default Register
