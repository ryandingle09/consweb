import useUser from '../lib/useUser'
import fetchJson from '../lib/fetchJson'
import Layout from '../components/Layout'
import withSession from '../lib/session'
import Head from 'next/head'

const Profile = (props) => {
  const { user } = useUser({ redirectTo: '/login' })

  if (!user || user.isLoggedIn === false) {
    return <Layout>loading...</Layout>
  }

  return (
    <Layout>
      <Head>
        <title>Profile | Consequence Test</title>
      </Head>
      <h1>Profile Details <a href={`profile/edit/${props.info.id}`}><button type="button" class="btn btn-link">Edit Profile</button></a></h1>
      <ul class="list-group list-group-flush">
        <li class="list-group-item">
          Username: {props.info.username}
        </li>
        <li class="list-group-item">
          Email: {props.info.email}
        </li>
        <li class="list-group-item">
          First Name: {props.info.first_name}
        </li>
        <li class="list-group-item">
          Last Name: {props.info.last_name}
        </li>
      </ul>
      <hr />
    </Layout>
  )
}

export const getServerSideProps = withSession(async function ({ req, res }) {
  const user = req.session.get('user');
  const url = `${process.env.API}user/details/`;

  let data  = await fetch(url, {
    method: 'GET',
    headers: { 'Authorization': `Bearer ${user.access}` }
  });
  
  if(data.status === 401) {
    const url = `${process.env.API}auth/token/refresh/`
    const user = req.session.get('user')
    const refresh = user.refresh

    const body = {
      refresh: refresh,
    }

    const { access } = await fetchJson(url, {
      headers: { 'Content-Type': 'application/json' },
      method: 'POST',
      body: JSON.stringify(body),
    });
    
    user['access'] = access
    req.session.set('user', user)
    await req.session.save()

    data = await fetch(url, {
      method: 'GET',
      headers: { 'Authorization': `Bearer ${user.access}` }
    });

    console.log(data.status);
  }

  const info = await data.json()

  return {
    props: {
      info,
    }
  }
})

export default Profile
