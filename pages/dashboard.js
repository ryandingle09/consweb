import useUser from '../lib/useUser'
import Layout from '../components/Layout'
import Head from 'next/head'

const Dashboard = () => {
  const { user } = useUser({ redirectTo: '/login' })

  if (!user || user.isLoggedIn === false) {
    return <Layout>loading...</Layout>
  }

  return (
    <Layout>
      <Head>
        <title>Dashboard | Consequence Test</title>
      </Head>
      <p>For transactions page</p>
      {/* <h1><a target="_blank" href="https://auth.truelayer-sandbox.com/?response_type=code&client_id=sandbox-ryandingle-6ca0b9&scope=info%20accounts%20balance%20cards%20transactions%20direct_debits%20standing_orders%20offline_access&redirect_uri=https://console.truelayer.com/redirect-page&providers=uk-ob-all%20uk-oauth-all%20uk-cs-mock" class="btn btn-primary btn-lg">CONNECT YOUR ANK</a></h1> */}
    </Layout>
  )
}

function githubUrl(login) {
  return `https://api.github.com/users/${login}`
}

export default Dashboard
