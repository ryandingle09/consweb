import { useState } from 'react'
import Head from 'next/head'
import withSession from '../../../lib/session'
import useUser from '../../../lib/useUser'
import Layout from '../../../components/Layout'
import FormEdit from '../../../components/FormEdit'
import fetchJson from '../../../lib/fetchJson'
import { useRouter } from 'next/router'

const Edit = (props) => {
  const router = useRouter()
  const [errorMsg, setErrorMsg, setSuccess] = useState('')

  async function handleSubmit(e) {
    e.preventDefault()

    const body = {
      username: e.currentTarget.username.value,
      email: e.currentTarget.email.value,
      first_name: e.currentTarget.first_name.value,
      last_name: e.currentTarget.last_name.value,
      id: e.currentTarget.id.value,
    }

    try {
        await fetchJson('/api/update/', {
          method: 'POST',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify(body),
        });

        router.push('/profile')
        
    } catch (error) {
      console.error('An unexpected error happened:', error)
      let errors = "";
      errors += (error.data.username) ? "Username: "+ error.data.username + "<br />" : '';
      errors += (error.data.email) ? "Email: "+ error.data.email + "<br />" : '';
      errors += (error.data.first_name) ? "First Name: "+ error.data.first_name + "<br />" : '';
      errors += (error.data.last_name) ? "Last Name: "+ error.data.last_name + "<br />" : '';
      errors += (error.data.password) ? "Password: "+ error.data.password + "<br />" : '';
      errors += (error.data.success) ? "Password: "+ error.data.success + "<br />" : '';
      setErrorMsg(errors)
    }
  }

  return (
    <Layout>
      <Head>
        <title>Update Profile | Consequence Test</title>
      </Head>
      <h1>
        <center>Update Profile</center>
      </h1>
      <div className="login">
        <FormEdit info={props.info} errorMessage={errorMsg} onSubmit={handleSubmit} />
      </div>
    </Layout>
  )
}

export const getServerSideProps = withSession(async function ({ req, res }) {
  const user = req.session.get('user');
  const url = `${process.env.API}user/details/`;

  let data  = await fetch(url, {
    method: 'GET',
    headers: { 'Authorization': `Bearer ${user.access}` }
  });
  
  if(data.status === 401) {
    const url = `${process.env.API}auth/token/refresh/`
    const user = req.session.get('user')
    const refresh = user.refresh

    const body = {
      refresh: refresh,
    }

    const { access } = await fetchJson(url, {
      headers: { 'Content-Type': 'application/json' },
      method: 'POST',
      body: JSON.stringify(body),
    });
    
    user['access'] = access
    req.session.set('user', user)
    await req.session.save()

    data = await fetch(url, {
      method: 'GET',
      headers: { 'Authorization': `Bearer ${user.access}` }
    });

    console.log(data.status);
  }

  const info = await data.json()

  return {
    props: {
      info,
    }
  }
})

export default Edit
