import fetchJson from '../../lib/fetchJson'
import withSession from '../../lib/session'

export default withSession(async (req, res) => {
  const url = `${process.env.API}auth/token/refresh/`
  const user = req.session.get('user')
  const refresh = user.refresh

  const body = {
    refresh: refresh,
  }

  try {
    const { access } = await fetchJson(url, {
      headers: { 'Content-Type': 'application/json' },
      method: 'POST',
      body: JSON.stringify(body),
    });
    
    user['access'] = access
    req.session.set('user', user)
    await req.session.save()
    res.json({
        'access': access
    })

  } catch (error) {
    const { response: fetchResponse } = error
    res.status(fetchResponse?.status || 500).json(error.data)
  }
})
