import withSession from '../../lib/session'

export default withSession(async (req, res) => {
  const user = req.session.get('user')
  const details = req.session.get('details')
  
  if (user) {
    res.json({
      isLoggedIn: true,
      ...details,
      ...user,
    })
  } else {
    res.json({
      isLoggedIn: false,
    })
  }
})
