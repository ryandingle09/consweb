import fetchJson from '../../lib/fetchJson'
import withSession from '../../lib/session'

export default withSession(async (req, res) => {
  const url = `${process.env.API}auth/login/`

  try {
    const { refresh, access } = await fetchJson(url, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(req.body),
    });
    const user = { isLoggedIn: true, refresh, access }

    req.session.set('user', user)

    await req.session.save()
    res.json(user)
    
  } catch (error) {
    console.log(error);
    const { response: fetchResponse } = error
    res.status(fetchResponse?.status || 500).json(error.data)
  }
})
