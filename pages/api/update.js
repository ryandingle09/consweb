import fetchJson from '../../lib/fetchJson'
import withSession from '../../lib/session'

export default withSession(async (req, res) => {
  const url = `${process.env.API}auth/update-user/`+ req.body.id + `/`
  const user = req.session.get('user');

  try {
    let data = await fetchJson(url, {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json', 'Authorization': `Bearer ${user.access}` },
      body: JSON.stringify(req.body),
    });
    
    if(data.status === 401) {
      const url = `${process.env.API}auth/token/refresh/`
      const user = req.session.get('user')
      const refresh = user.refresh
  
      const body = {
        refresh: refresh,
      }
  
      const { access } = await fetchJson(url, {
        headers: { 'Content-Type': 'application/json' },
        method: 'POST',
        body: JSON.stringify(body),
      });
      
      user['access'] = access
      req.session.set('user', user)
      await req.session.save()
  
      data = await fetchJson(url, {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json', 'Authorization': `Bearer ${user.access}` },
        body: JSON.stringify(req.body),
      });
    }
      
  let message = {
    data: {
      success: ['Successfully Update!']
    }
  }

  res.json(message)
    
  } catch (error) {
    // console.log(error);
    const { response: fetchResponse } = error
    res.status(fetchResponse?.status || 500).json(error.data)
  }
});
