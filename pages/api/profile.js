import fetchJson from '../../lib/fetchJson'
import withSession from '../../lib/session'

export default withSession(async (req, res) => {
  const call = async(req, res) => {
    const url = `${process.env.API}user/details/`;
    const user = req.session.get('user');
    
    const details = await fetchJson(url, {
      method: 'GET',
      headers: { 'Authorization': `Bearer ${user.access}` }
    });

    user['first_name'] = details.first_name
    user['last_name'] = details.last_name
    user['username'] = details.email
    user['email'] = details.email

    req.session.set('user', user);
    await req.session.save();
    res.json(details);
  }

  try {
    return call(req, res)
  } catch (error) {
    const { response: fetchResponse } = error

    console.log(fetchResponse.status);

    if(fetchResponse.status === 401) {
      await fetchJson('/api/refresh')
      return call(req, res)
    }

    res.status(fetchResponse?.status || 500).json(error.data)
  }
});
