import React from 'react'
import Link from 'next/link'
import useUser from '../lib/useUser'
import { useRouter } from 'next/router'
import fetchJson from '../lib/fetchJson'

const Header = () => {
  const { user, mutateUser } = useUser()
  const router = useRouter()
  return (
    <header>
      <nav class="navbar navbar-expand-lg navbar-light bg-light navbar-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="/">Consquence</a>
              <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                  {!user?.isLoggedIn && (
                  <>
                  <li class="nav-item">
                    <a class="nav-link" href="/login">Login</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="/register">Register</a>
                  </li>
                  </>
                  )}
                  {user?.isLoggedIn && (
                  <>
                  <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="/">Home</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="/dashboard">Dashoard</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="/profile">Profile</a>
                  </li>
                  {/* <li class="nav-item">
                    <a class="nav-link" href="/dashboard">Cards</a>
                  </li> */}
                  <li class="nav-item">
                    <a class="nav-link" href="/accounts">Accounts</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="/api/logout"
                      onClick={async (e) => {
                        e.preventDefault()
                        mutateUser(
                          await fetchJson('/api/logout', { method: 'POST' }),
                          false
                        )
                        router.push('/login')
                      }}>Logout</a>
                  </li>
                  </>
                  )}
                </ul>
              </div>
          </div>
      </nav>
    </header>
  )
}

export default Header
