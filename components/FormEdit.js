import React from 'react'
import PropTypes from 'prop-types'
import parse from 'html-react-parser';

const Form = ({ errorMessage, onSubmit , info}) => (
  <form onSubmit={onSubmit}>
    {errorMessage && <>
    <div class="alert alert-danger" role="alert">
      {parse(errorMessage)}
    </div>
    <hr />
    </>
    }
    <div class="mb-4">
      <label for="exampleInputEmail1" class="form-label">Username</label>
      <input type="text" name="username" defaultValue={`${info.username}`} class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" />
      <div id="emailHelp" class="form-text">We'll never share your username with anyone else.</div>
    </div>
    <div class="mb-4">
      <label for="exampleInputEmail1" class="form-label">Email Address</label>
      <input type="email" name="email" defaultValue={info.email}  class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" />
      <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
    </div>
    <div class="mb-4">
      <label for="exampleInputEmail1" class="form-label">First Name</label>
      <input type="text" name="first_name" defaultValue={info.first_name}  class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" />
    </div>
    <div class="mb-4">
      <label for="exampleInputEmail1" class="form-label">Last Name</label>
      <input type="text" name="last_name" defaultValue={info.last_name}  class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" />
    </div>
      <input type="hidden" name="id" value={info.id} />

    <button type="submit" class="btn btn-primary btn-lg">Update</button>
  </form>
)

export default Form

Form.propTypes = {
  errorMessage: PropTypes.string,
  success: PropTypes.string,
  info: PropTypes.array,
  onSubmit: PropTypes.func,
}
