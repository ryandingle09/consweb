import React from 'react'
import PropTypes from 'prop-types'

const Form = ({ errorMessage, onSubmit }) => (
  <form onSubmit={onSubmit}>
    <div class="mb-4">
      <label for="exampleInputEmail1" class="form-label">Username</label>
      <input type="text" name="username" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" />
    </div>

    <div class="mb-4">
      <label for="exampleInputEmail1" class="form-label">Password</label>
      <input type="password" name="password" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" />
    </div>

    <button type="submit" class="btn btn-primary btn-lg">Login</button>
    <br />
    <hr />
    {errorMessage && <>
      <div class="alert alert-danger" role="alert">
        {errorMessage}
      </div>
    </>
    }
  </form>
)

export default Form

Form.propTypes = {
  errorMessage: PropTypes.string,
  onSubmit: PropTypes.func,
}
